import React, { Component } from 'react';
import Home from './modules/home';
import Detail from './modules/detail';
import Header from './components/header';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

/**
 * Clase principal del proyecto
 * @export
 * @class     App
 * @date      06/06/2022
 * @lastDate  24/06/2022
 * @extends   {Component}
 * @autor     Sebastian Saldarriaga
 */
export default class App extends Component {
  constructor(props) {
    super(props);

    this.cmpHeader = React.createRef();
  }


  /**
   * Función para cuando se precione enter en el campo de texto del header inmediatamente pase de endPoint y consulte los items relacionados
   * @memberof  App
   * @param     {*} event -> Acciones que se tienen al momento de hacer cualquier movimiento con el campo de texto
   */
  pressEnter = event => {
    const { keyCode } = event;

    if (Number(keyCode) === 13) {
      localStorage.setItem("searchHeader", this.cmpHeader.current.getValue());

      return window.location.assign("/items");
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route exact path='/' element={<Header ref={this.cmpHeader} onKey={this.pressEnter} />} />
          <Route path='/items' element={<Home />} />
          <Route path='/items/:id' element={<Detail />} />
        </Routes>
      </BrowserRouter>
    );
  }
}
