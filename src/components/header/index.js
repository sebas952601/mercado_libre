import { Component } from 'react';
import Text from '../text';
import Image from '../image';
import logo from '../../images/logo.png';
import './styleHeader.scss';

/**
 * Componente para el header en el cual visualizamos:
 *  El logo de la compañia
 *  Barra de busqueda
 * Este componente sera el principal, ya que todas las paginas contienen este header
 * @export
 * @class     Header
 * @date      06/06/2022
 * @lastDate  24/06/2022
 * @autor     Sebastian Saldarriaga
 * @extends   {Component}
 */
export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      onKey: props.onKey,
    };
  }


  /**
   * Función que nos va a ayudar a cambiar el valor del campo de texto
   * @memberof  Header
   * @param     {*} value
   */
  setValue = value => {
    this.setState({ value: typeof value === 'object' ? value.target.value : value });
  }

  /**
   * Función que nos va a ayudar a obtener el valor que tiene el campo de texto
   * @memberof Header
   */
  getValue = () => {
    const { value } = this.state;
    return value;
  }

  render() {
    const { onKey, value } = this.state;

    return (
      <div className='cmp-header__container'>
        <Image img={logo} width="50px" height="50px" styleImg={{ paddingLeft: '20px' }} />
        <Text value={value} placeholder="Nunca dejes de buscar" onChange={this.setValue} onKey={onKey} />
      </div>
    );
  }
}
