/**
 * Componente para las imagenes en el cual visualizamos:
 *  Imagen/Icono con las medidas/propiedades dadas
 * Este componente nos ayuda a poner imagenes donde lo deseemos 
 * @export
 * @return    {*} 
 * @date      06/06/2022
 * @lastDate  24/06/2022
 * @autor     Sebastian Saldarriaga
 * @param     {*} { 
 *  img -> Imagen que se va a visualizar,
 *  width -> Dimensión de ancho dada a la imagen,
 *  height -> Dimensión de largo dada a la imagen,
 *  styleImg -> Estilos que se le quieran dar a la imagen,
 *  alt: En caso de que no se visualize la imagen aparece este texto,
 *  onClick -> en caso de que tenga una acción se le da en este parametro 
 * }
 */
export default function Image({ img, alt, width, height, styleImg, onClick }) {
  return <img src={img} alt={alt} width={width} height={height} style={styleImg} onClick={onClick} />;
}
