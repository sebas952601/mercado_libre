import { Form } from 'react-bootstrap';
import './styleText.scss';

/**
 * Componente para los campos de texto, en el cual visualizamos:
 *  Un label/texto donde se define para que es(descripción) el campo de texto
 *  Una caja de texto donde podremos escribir lo que se necesite
 * @export
 * @return    {*} 
 * @date      06/06/2022
 * @lastDate  24/06/2022
 * @autor     Sebastian Saldarriaga
 * @param     {*} {
 *  style -> Estilos dados en los props,
 *  value -> valor que se va a mostrar en el caja de texto
 *  label -> Texto de ayuda para saber que se va a dijitar en el campo de texto,
 *  placeholder -> si se desea que se tenga un ejemplo en el campo de texto se le pone este campo,
 *  onKey -> Metodo para cuando se desee una acción en el momento de hacer enter sobre el componente,
 *  onChange -> Parametro que nos va a servir para cuando se haga un cambio en el componente y se desee hacer algo,
 * }
 */
export default function Text({ label, style, placeholder, onChange, onKey, value = '' }) {
  return (
    <div className='cmp-text__container'>
      <Form.Group className='cmp-text__container-form'>
        <Form.Label>{label}</Form.Label>
        <Form.Control value={value} placeholder={placeholder} className='cmp-text__text-form' onChange={onChange} onKeyUp={onKey} />
      </Form.Group>
    </div>
  );
}
