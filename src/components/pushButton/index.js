import { Button } from "@mui/material";

/**
 * Componente para los botones, en el cual visualizamos:
 *  Boton con los estilos y los componentes que se manden en los props
 * @export
 * @return    {*} 
 * @date      06/06/2022
 * @lastDate  10/06/2022
 * @autor     Sebastian Saldarriaga
 * @param     {*} {
 *  style -> Estilos dados
 *  text -> Texto/label que contendra el boton,
 *  handleClick -> Prop para definir la función/click del boton,
 *  disabled -> Prop para definir si el boton esta desabilitado o no,
 * }
 */
export default function PushButton({ text, disabled, handleClick, style }) {
  return (
    <Button variant="contained" onClick={handleClick} disabled={disabled} style={style}>
      <span>{text}</span>
    </Button>
  );
}