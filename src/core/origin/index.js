import { Component } from 'react';

/**
 * Componente usado para definit funciones globales que sean necesarias en nuestro proyecto
 * @export
 * @class     Origin
 * @date      06/06/2022
 * @lastDate  24/06/2022
 * @extends   {Component}
 * @autor     Sebastian Saldarriaga
 */
export default class Origin extends Component {
  /**
   * Función para consultar a la base de datos o endPoint que se suministre
   * @memberof Origin
   */
  post = async ({ endPoint }) => {
    const response = await fetch(`http://localhost:8080/${endPoint}`);
    return response;
  }
}
