/* eslint-disable array-callback-return */
import React from 'react';
import Origin from '../../core/origin';
import { Link } from 'react-router-dom';
import Image from '../../components/image';
import Header from '../../components/header';
import './styleHome.scss';

/**
 * Modulo en el cual veremos la cantidad de productos que se consulten en la barra de busqueda, en el cual encontramos:
 *  Listado de productos similares a la busqueda
 *  Imagen del producto
 *  Descripción
 *  Valor del producto
 *  Precio de las cuotas x 36
 *  Si aplica envio gratis aparece el mensaje
 *  Unidades disponibles
 * @export
 * @class     Home
 * @extends   {Origin}
 * @date      07/06/2022
 * @lastDate  27/06/2022
 * @autor     Sebastian Saldarriaga
 */
export default class Home extends Origin {
  constructor(props) {
    super(props);

    this.cmpHeader = React.createRef();

    this.state = {
      products: [],
    }
  }

  /**
   * Función a la cual entra inmediatamente después de renderizar...
   * En esta podemos realizar acciones despues de renderizar
   * Para este caso necesitamos que nos consulte la información que vamos a mostrar en el navegador
   * @memberof Home
   */
  componentDidMount = () => {
    this.txtHeader = this.cmpHeader.current;
    
    const search = localStorage.getItem("searchHeader");
    this.txtHeader.setValue(search);
    this.postData(search);
  }

  /**
   * Función para realizar una consulta a la ip correspondiente, y en caso de fallar algo nos indica el error correspondiente
   * @memberof  Home
   * @param     {*} search -> Almacena el valor ingresado en el header, que va a ser por el cual vamos a realizar la consulta
   */
  postData = async (search) => {
    this.post({ endPoint: `products/items?search=${search}` })
      .then(async responseData => {
        if (responseData.ok) {
          const response  = JSON.parse(await responseData.text());
          const { items } = JSON.parse(response.body.regs);

          this.setState({ products: items });
        }
      })
      .catch(error => console.log('Hubo un problema con la petición Fetch:' + error.message));
  }

  /**
   * Función la cual nos brinda la posibilidad de darle formato a un valor entero
   * y pasarlo a un formato de pesos (1000000 => $ 1.000.000)
   * @return  {*} -> nos retorna el valor con el formato deseado
   * @param   {*} num -> valor entero el cual vamos a dar el respectivo formato
   */
  currencyFormat = (num) => {
    const value = `${Math.round(num)}`;
    return `$ ${value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
  };

  /**
   * Función para cuando presione enter en el header realice una nueva busqueda
   * En caso de que el header este sin texto se devuelve a la vista principal
   * @memberof  Home
   * @param     {*} event -> Acciones que se tienen al momento de hacer cualquier movimiento con el campo de texto
   */
  pressEnter = event => {
    if (this.txtHeader.getValue() === '') {
      return window.location.assign("/");;
    }
    
    // Para este caso necesitamos que se haga algo cuando se precione enter que es el codigo 13
    const { keyCode } = event;
    if (Number(keyCode) === 13) {
      localStorage.setItem("searchHeader", this.txtHeader.getValue());

      this.postData(this.txtHeader.getValue());
    }
  }

  render() {
    const { products } = this.state;

    return (
      <div className='home__container'>
        <Header ref={this.cmpHeader} onKey={this.pressEnter} />

        <div className='home__contanier-search'>
          <ol>
            {products.map(({ id, title, price, picture, decimals, free_shipping }, index) => (
              <li key={index}>
                <div className='home__elements'>
                  <Image img={picture} width="160px" height="160px" styleImg={{ padding: '0 24px' }} />

                  <Link to={`/items/:${id}`} className='home__link'>
                    <div className='home__elements-details'>
                      <ol>
                        <li>{title}</li>
                        <li className='home__elements-label-value'>{this.currencyFormat(price.amount)}</li>
                        <li>en 36x {this.currencyFormat(price.amount / 36)}</li>
                        {free_shipping && <li style={{ color: '#00a650' }}>Envío gratis</li>}
                      </ol>
                    </div>
                  </Link>
                  
                  <label className='home__available'>{price.decimals} unidades</label>
                </div>
              </li>
            ))}
          </ol>
        </div>
      </div>
    );
  }
}
