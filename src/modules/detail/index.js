/* eslint-disable array-callback-return */
import React from 'react';
import Origin from '../../core/origin';
import Image from '../../components/image';
import Header from '../../components/header';
import arrowLeft from '../../images/arrow.png';
import PushButton from '../../components/pushButton';
import './styleDetail.scss';

/**
 * Modulo en el cual veremos mas a detalle nuestro producto en el cual encontramos:
 *  Nombre del producto
 *  Descripción breve
 *  Precio
 *  Imagen del producto
 * @export
 * @class     Detail
 * @date      09/06/2022
 * @lastDate  24/06/2022
 * @extends   {Component}
 * @autor     Sebastian Saldarriaga
 */
export default class Detail extends Origin {
  constructor(props) {
    super(props);

    this.state = {
      arrowLeft,
      product:  {},
      id:       window.location.href.slice(29),
    };
  }

  /**
   * Función a la cual entra inmediatamente después de renderizar...
   * En esta podemos realizar acciones despues de renderizar
   * Para este caso necesitamos que nos consulte la información que vamos a mostrar en el navegador
   * @memberof Detail
   */
  componentDidMount = () => {
    this.postData();
  }

  /**
   * Función para realizar una consulta a la ip correspondiente, y en caso de fallar algo nos indica el error correspondiente
   * @memberof Detail
   */
  postData = async () => {      
    const { id } = this.state;

    this.post({ endPoint: `products/items/:?id=${id}` })
      .then(async responseData => {
        if (responseData.ok) {
          const response  = JSON.parse(await responseData.text());
          const objRegs   = JSON.parse(response.body.regs);

          this.setState({ product: objRegs });
        }
      })
      .catch(error => console.log('Hubo un problema con la petición Fetch:' + error.message));
  }

  /**
   * Función la cual nos brinda la posibilidad de darle formato a un valor entero
   * y pasarlo a un formato de pesos (1000000 => $ 1.000.000)
   * @return  {*} -> nos retorna el valor con el formato deseado
   * @param   {*} num -> valor entero el cual vamos a dar el respectivo formato
   */
  currencyFormat = (num) => {
    const value = `${Math.round(num)}`;
    return `$ ${value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
  };

  /**
   * Función para cuando se de click en la imagen regrese a la vista de los productos
   * @memberof Detail
   */
  eventKey = () => {
    return window.location.assign("/items");
  }

  render() {
    const { arrowLeft, product } = this.state;
    if (product.item) {
      const { condition, description, picture, price, sold_quantity, title } = product.item;

      return (
        <div className='detail__container'>
          <Header ref={this.cmpHeader} onKey={() => console.log("preciono enter")} />

          <Image img={arrowLeft} width="30px" styleImg={{ margin: '30px', cursor: 'url(imagen1.gif), url(cursor.cur), pointer' }} onClick={this.eventKey} />

          <div className='detail__contanier-search'>
            <div className='detail__container-img'>
              <Image img={picture} width="400px" styleImg={{ marginLeft: '30px', alignContent: 'center' }} />

              <div className='detail__container-price-name-product'>
                <label>{condition} - {sold_quantity} vendidos</label>
                
                <h3>{title}</h3>
                
                <h1>{this.currencyFormat(price.amount)}</h1>
                
                <PushButton text="Comprar" style={{ width: '200px' }} />
              </div>
            </div>

            <div className='detail__container-description'>
              <h2>Descripción del producto</h2>
              <label>{description}</label>
            </div>
          </div>
        </div>
      );
    }
  }
}
